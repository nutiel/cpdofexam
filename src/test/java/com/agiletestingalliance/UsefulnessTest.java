package com.agiletestingalliance;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class UsefulnessTest {
    
    @Test
    public void runDescTest() throws Exception {

        String str = "DevOps is about transformation, about building quality in, improving productivity and about automation in Dev, Testing and Operations. <br> <br> CP-DOF is a one of its kind initiative to marry 2 distinct worlds of Agile and Operations together. <br> <br> <b>CP-DOF </b> helps you learn DevOps fundamentals along with Continuous Integration and Continuous Delivery and deep dive into DevOps concepts and mindset.";
        
        String t = new Usefulness().desc();

        assertEquals("Usefulnessclass", str, t);
    }

    @Test
    public void runFunctionWFTest(){
        new Usefulness().functionWF();
    }
}
