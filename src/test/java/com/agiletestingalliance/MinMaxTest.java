package com.agiletestingalliance;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MinMaxTest {
    
    @Test
    public void runMinMaxTest() throws Exception {
        int k = new MinMax().f(1,2);

        assertEquals("ftest", 2, k);

        k = new MinMax().f(3,2);

        assertEquals("ftest", 3, k);

        String str = new MinMax().bar("Hello");

        assertEquals("testbar", "Hello", str);

        str = new MinMax().bar("");
        
        assertEquals("testbar", "", str);
    }
}
